const Viva = require('../vivagraphLib/vivagraph.js')
let socketIO

//Initialize
backendAddres = 'http://localhost:5000'
let graph = Viva.Graph.graph();

//Initialize SocketIO
try {
    socketIO = require('socket.io-client')(backendAddres)
    console.log('Connected to server...')
    socketIO.on('message', function(txHash, trunkHash, branchHash) {
        /*console.log(
            'New TX: ' + txHash + '\n',
            'TrunkTX: ' + trunkHash + '\n',
            'BranchTX: ' + branchHash + '\n',
        )
        */
        graph.addNode(txHash, {url : 'https://thetangle.org/transaction/' + txHash})
        graph.addNode(trunkHash, {url : 'https://thetangle.org/transaction/' + trunkHash})
        graph.addNode(branchHash, {url : 'https://thetangle.org/transaction/' + branchHash})
        graph.addLink(txHash, trunkHash)
        graph.addLink(txHash, branchHash)
    })
} catch(err) {
    console.log(err)
}      

//Main function called by Index.html
onLoad = () => {

    var layout = Viva.Graph.Layout.forceDirected(graph, {
       springLength : 50,
       springCoeff : 0.0008,
       dragCoeff : 0.005,
       gravity : -5.5
    });

    //WebGL rendering looks better than SVG
    //var graphics = Viva.Graph.View.svgGraphics();

    // You can pin/unpin node any time you want:
    /*
    graphics.node(function (node) {
        var ui = Viva.Graph.svg("rect")
             .attr("width", 10)
             .attr("height", 10)
             .attr("fill", "#00a2e8");
        ui.addEventListener('click', function () {
            // toggle pinned mode
            layout.pinNode(node, !layout.isNodePinned(node));
        });
        return ui;
    });
    */

    var graphics = Viva.Graph.View.webglGraphics();

    //Manage mouse events (click and hover)
    var events = Viva.Graph.webglInputEvents(graphics, graph);
    events.click(function (node) {
        //console.log(node.data['url']);
        window.open(node.data['url']);
    }).mouseEnter(function (node) {
        //console.log('Mouse entered node: ' + node.id);
        document.getElementById('tx-hash').innerText = 'TX HASH: ' + node.id
    })


    var renderer = Viva.Graph.View.renderer(graph, {
            layout    : layout,
            graphics  : graphics,
            container : document.getElementById('graphContainer')
        });

    renderer.run();
}
