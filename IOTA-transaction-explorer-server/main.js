const IOTA = require('iota.lib.js')
const zmq = require('zeromq')
const zmqSub = zmq.socket('sub')
const socketIo = require('socket.io')
const http = require('http')


const readline = require('readline').createInterface({
  input: process.stdin,
  output: process.stdout
})

//Initialize variables
const nodeAddress = 'https://my-iota-node.com:14267'
let tx_hash_array = ['9VZZWALEROUFFTZTRSVJQLFOW9SIZADSHWIAZJSLAVFIDKLGZJLQQWWUQZIJTWTKNYPU9GFPZCRWZ9999']
let i = 0

// ------------------- START SERVER ------------------- 
server = http.createServer(onRequest).listen(5000)

//Create GET fuction on server
function onRequest(req, res) {
  res.writeHead(200, {'Content-Type': 'application/json'});
  res.end('Socket server is active');
}

//Open socket connection
try{
  //Activate socket instance on server
  socketOnServer = socketIo.listen(server)
  socketOnServer.on('connection', (socket) => {
    //console.log('Client connected...')
    //socketOnServer.emit('message', 'You are connected!')
  })
} catch(err){
  console.log(String(err))
}

// Create IOTA instance
var iota = new IOTA({
  provider: nodeAddress
});


//Get TX information from node
checkTransactions = (itr) => {
  if (i < itr){
    iota.api.getTransactionsObjects(tx_hash_array, ((err, res) => {
      //console.log('Array dimension: ' + String(tx_hash_array.length))
      if (err) {
        console.log(err)
      } else {
        tx_hash_array.length = 0
        //console.log(res)
        for (n = 0; n < res.length; n++){
          tx_hash_array.push(res[n]['trunkTransaction'])
          tx_hash_array.push(res[n]['branchTransaction'])
          socketOnServer.emit('message', res[n]['hash'], res[n]['trunkTransaction'], res[n]['branchTransaction'])
        }
      }
      console.log('Array dimension: ' + String(tx_hash_array.length))
      i++
      checkTransactions(itr)
    }))
  } else {
    console.log('Maximum number of iterations reached: ' + i)
  }
}

// ------------------- START TRANSACTION CHECK -------------------
init = () => {
  readline.question(`How many iterations? `, (itr) => {
    checkTransactions(itr)
    readline.close()
  })
}

init()
