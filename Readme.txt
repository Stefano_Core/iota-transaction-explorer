Procedure: 
1. Clone or download the repository on your workstation
2. With two terminals, open both folders, as they're two separated projects:
	IOTA-transaction-visualizer-server
	IOTA-transaction-visualizer-client
3. In "IOTA-transaction-visualizer-client" folder, execute the following commands:
	npm install
	npm install parcel
	npm run build
4. In "IOTA-transaction-visualizer-server" folder, execute the following commands:
	npm install
	npm start
5. The server will ask how many iterations/step backward have to be done to retrieve the previous transactions; indicatively, try with 10 iterations
6. Then, the server will start to send data to client 
6. Automatically, Parcel should open the webpage @ localhost:1234 where the graphical representation of the incoming IOTA transaction will start appearing
7. Press CTRL+C to stop client and server